; Copyright 2010 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET   Hdr:AudioDevice
        GET   Hdr:MixerDevice

; McBSP registers - relative to L4_McBSP*

MCBSPLP_DRR           *    &00
MCBSPLP_DXR           *    &08
MCBSPLP_SPCR2         *    &10
MCBSPLP_SPCR1         *    &14
MCBSPLP_RCR2          *    &18
MCBSPLP_RCR1          *    &1C
MCBSPLP_XCR2          *    &20
MCBSPLP_XCR1          *    &24
MCBSPLP_SRGR2         *    &28
MCBSPLP_SRGR1         *    &2C
MCBSPLP_MCR2          *    &30
MCBSPLP_MCR1          *    &34
MCBSPLP_RCERA         *    &38
MCBSPLP_RCERB         *    &3C
MCBSPLP_XCERA         *    &40
MCBSPLP_XCERB         *    &44
MCBSPLP_PCR           *    &48
MCBSPLP_RCERC         *    &4C
MCBSPLP_RCERD         *    &50
MCBSPLP_XCERC         *    &54
MCBSPLP_XCERD         *    &58
MCBSPLP_RCERE         *    &5C
MCBSPLP_RCERF         *    &60
MCBSPLP_XCERE         *    &64
MCBSPLP_XCERF         *    &68
MCBSPLP_RCERG         *    &6C
MCBSPLP_RCERH         *    &70
MCBSPLP_XCERG         *    &74
MCBSPLP_XCERH         *    &78
MCBSPLP_RINTCLR       *    &80
MCBSPLP_XINTCLR       *    &84
MCBSPLP_ROVFLCLR      *    &88
MCBSPLP_SYSCONFIG     *    &8C
MCBSPLP_THRSH2        *    &90
MCBSPLP_THRSH1        *    &94
MCBSPLP_IRQSTATUS     *    &A0
MCBSPLP_IRQENABLE     *    &A4
MCBSPLP_WAKEUPEN      *    &A8
MCBSPLP_XCCR          *    &AC
MCBSPLP_RCCR          *    &B0
MCBSPLP_XBUFFSTAT     *    &B4
MCBSPLP_RBUFFSTAT     *    &B8
MCBSPLP_SSELCR        *    &BC
MCBSPLP_STATUS        *    &C0

; McBSP sidetone registers - relative to L4_McBSP*S

ST_SYSCONFIG          *    &10
ST_IRQSTATUS          *    &18
ST_IRQENABLE          *    &1C
ST_SGAINCR            *    &24
ST_SFIRCR             *    &28
ST_SSELCR             *    &2C

; TWL/TPS audio IIC address
TPSAUDIO_IIC   * &49

; TWL/TPS audio registers

CODEC_MODE              *  &01
OPTION                  *  &02
MICBIAS_CTL             *  &04
ANAMICL                 *  &05
ANAMICR                 *  &06
AVADC_CTL               *  &07
ADCMICSEL               *  &08
DIGMIXING               *  &09
ATXL1PGA                *  &0A
ATXR1PGA                *  &0B
AVTXL2PGA               *  &0C
AVTXR2PGA               *  &0D
AUDIO_IF                *  &0E
VOICE_IF                *  &0F
ARXR1PGA                *  &10
ARXL1PGA                *  &11
ARXR2PGA                *  &12
ARXL2PGA                *  &13
VRXPGA                  *  &14
VSTPGA                  *  &15
VRX2ARXPGA              *  &16
AVDAC_CTL               *  &17
ARX2VTXPGA              *  &18
ARXL1_APGA_CTL          *  &19
ARXR1_APGA_CTL          *  &1A
ARXL2_APGA_CTL          *  &1B
ARXR2_APGA_CTL          *  &1C
ATX2ARXPGA              *  &1D
BT_IF                   *  &1E
BTPGA                   *  &1F
BTSTPGA                 *  &20
EAR_CTL                 *  &21
HS_SEL                  *  &22
HS_GAIN_SET             *  &23
HS_POPN_SET             *  &24
PREDL_CTL               *  &25
PREDR_CTL               *  &26
PRECKL_CTL              *  &27
PRECKR_CTL              *  &28
HFL_CTL                 *  &29
HFR_CTL                 *  &2A
ALC_CTL                 *  &2B
ALC_SET1                *  &2C
ALC_SET2                *  &2D
BOOST_CTL               *  &2E
SOFTVOL_CTL             *  &2F
DTMF_FREQSEL            *  &30
DTMF_TONEXT1H           *  &31
DTMF_TONEXT1L           *  &32
DTMF_TONEXT2H           *  &33
DTMF_TONEXT2L           *  &34
DTMF_TONOFF             *  &35
DTMF_WANONOFF           *  &36
CODEC_RX_SCRAMBLE_H     *  &37
CODEC_RX_SCRAMBLE_M     *  &38
CODEC_RX_SCRAMBLE_L     *  &39
APLL_CTL                *  &3A
DTMF_CTL                *  &3B
DTMF_PGA_CTL2           *  &3C
DTMF_PGA_CTL1           *  &3D
MISC_SET_1              *  &3E
PCMBTMUX                *  &3F
RX_PATH_SEL             *  &43
VDL_APGA_CTL            *  &44
VIBRA_CTL               *  &45
VIBRA_SET               *  &46
ANAMIC_GAIN             *  &48
MISC_SET_2              *  &49

; McBSP DMA request IDs

McBSP1_DMA_TX * 30
McBSP1_DMA_RX * 31
McBSP2_DMA_TX * 32
McBSP2_DMA_RX * 33
McBSP3_DMA_TX * 16
McBSP3_DMA_RX * 17
McBSP4_DMA_TX * 18
McBSP4_DMA_RX * 19
McBSP5_DMA_TX * 20
McBSP5_DMA_RX * 21

; McBSP IRQ numbers (ignoring legacy split TX/RX IRQs)
McBSP1_IRQ    * 16
McBSP2_IRQ    * 17
McBSP3_IRQ    * 22
McBSP4_IRQ    * 23
McBSP5_IRQ    * 27
ST_McBSP2_IRQ * 4
ST_McBSP3_IRQ * 5

; Audio device
                       ^   0, a1
; Public bits
AudioDevice            #   HALDevice_Audio_Size_2
; Private bits
AudioRegs              #   4 ; L4_McBSP*_Log
AudioWorkspace         #   4 ; HAL workspace pointer
AudioMode              #   4 ; Softcopy of CODEC_MODE register
Audio_DeviceSize       *   :INDEX: @

; Mixer device
                        ^       0
MixerChannel_HeadsetOut #       1 ; Stereo headset output
MixerChannel_Predriver  #       1 ; Stereo speaker predriver output
MixerChannel_HandsFree  #       1 ; Stereo hands-free output
MixerChannel_CarkitOut  #       1 ; Stereo carkit output
MixerChannel_System     #       1 ; Stereo sound data generated by computer
MixerChannel_HeadsetMic #       1 ; Mono headset mic
MixerChannel_AuxInput   #       1 ; Stereo FM/aux input
MixerChannel_CarkitMic  #       1 ; Mono carkit mic
MixerChannels           *       :INDEX: @

                       ^   0, a1
; Public bits
MixerDevice            #   HALDevice_Mixer_Size + 4 ; +4 for API 0.1
; Private bits
MixerSettings          #   8 * MixerChannels
MixerHeadsetGain       #   2 ; Cached HS_GAIN_SET value
MixerPredriverGain     #   2 ; Cached PREDL_CTL, PREDR_CTL regs
MixerHandsFreeGain     #   2 ; Cached HFL_CTL, HFR_CTL regs
MixerCarkitOutGain     #   2 ; Cached PRECKL_CTL, PRECKR_CTL regs
MixerSystemGain        #   2 ; Cached ARXL2PGA, ARXR2PGA regs
MixerSystemGain2       #   2 ; Cached ARXL2_APGA_CTL, ARXR2_APGA_CTL regs
MixerDisableFlags      #   1 ; Copy of BoardConfig_MixerChans, for convenience
                       #   3 ; Spare
Mixer_DeviceSize       *   :INDEX: @

Audio_WorkspaceSize    *    (Audio_DeviceSize + Mixer_DeviceSize)

        END
